import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { PostData } from '../share/interfaces/post-data';
import { debounceTime, filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WebApiService {

  protected debounceTime = 1000;
  protected webApiSubject = new Subject<PostData | string>();
  public postResult: Observable<PostData | string> = this.webApiSubject.asObservable().pipe(
    debounceTime(this.debounceTime),
    filter((data: PostData) => data.controlValid),
  );

  constructor() {
  }

  /**
   * отправляем данные на сервер
   * @param data: PostData
   */
  public postFormToServer(data: PostData | string): void {
    this.webApiSubject.next(data);
  }
}
