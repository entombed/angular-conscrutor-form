export type ConfigTypeField = 'string' | 'number' | 'calendar' | 'dropdown' | 'mask' | 'radio' | 'checkbox';
