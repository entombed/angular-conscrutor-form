export enum ValidationStatus {
  invalid = 'INVALID',
  valid = 'VALID',
}
