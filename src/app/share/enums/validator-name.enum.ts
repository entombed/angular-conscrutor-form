export type ValidatorName = 'min' | 'max' | 'required' | 'email' | 'maxLength' | 'minLength' | 'pattern';
