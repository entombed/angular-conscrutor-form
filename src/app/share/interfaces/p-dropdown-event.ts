export interface PDropdownEvent {
  event: number | string | null;
  originalEvent: KeyboardEvent;
}
