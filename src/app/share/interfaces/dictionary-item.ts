export interface DictionaryItem {
  value: number | string | null;
  label: string;
}
