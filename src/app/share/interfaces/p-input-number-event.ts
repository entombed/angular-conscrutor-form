export interface PInputNumberEvent {
  event: number | null;
  originalEvent: KeyboardEvent;
}
