export interface PostData {
  value: any; // значение
  controlName: string; // имя форм контроля
  controlType: string; // типа поля (добавил на всякий случай для бека, возможно и лишним будет)
  formName: string; // идентификатор формы для бека
  controlValid: boolean; // нужно что бы определить отправляем value или нет
}
