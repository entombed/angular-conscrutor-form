import { ConfigTypeField } from '../types/config-type-field.type';
import { ValidatorItem } from './validator-item';
import { DictionaryItem } from './dictionary-item';

export interface FormConfigItem {
  label: string; // название поля, отображается в форме
  value: any; // значение которое необходимо установить при создании формы
  type: ConfigTypeField; // тип элемента, на основаниии которого рисуем разные поля
  name: string; // назавание FormControl
  width?: number; // ширина поля (используем 12 колоночный подход)
  validators?: ValidatorItem[]; // массив валидаторов для FormControl
  list?: DictionaryItem[]; // справочник для выпадающего списка и для чекбоксов, содержит label и value
  mask?: string; // маска для полей типа маска
}
