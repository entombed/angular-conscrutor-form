import { ValidatorName } from '../enums/validator-name.enum';

export interface ValidatorItem {
  name: ValidatorName; // имя валидатора
  value?: number | string | null; // параметр который передается в валидатор (опциональное значение)
}


