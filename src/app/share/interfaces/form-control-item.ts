import { FormControl } from '@angular/forms';

export interface FormControlItem {
  [key: string]: FormControl;

}
