import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { ConfigTypeField } from './share/types/config-type-field.type';
import { FormConfigItem } from './share/interfaces/form-config-item';
import { FormControlItem } from './share/interfaces/form-control-item';
import { WebApiService } from './services/web-api.service';
import { PostData } from './share/interfaces/post-data';
import { formConfig, formName } from './models/form-config.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {

  protected formName: string = formName;

  public formConfig: FormConfigItem[] = formConfig;
  public form: FormGroup;
  public formControls: FormControlItem;

  constructor(
    protected webApiService: WebApiService
  ) {
  }

  ngOnInit(): void {
    this.initForm();
    this.initFetchResultFromServer();
  }

  /**
   * создаем форму
   * @protected
   */
  protected initForm(): void {
    this.formControls = this.buildingFormControls(this.formConfig);
    this.form = new FormGroup(this.formControls);
  }

  /**
   * запускаем файковую отправку данных на сервер с задержкой
   * @protected
   */
  protected initFetchResultFromServer(): void {
    this.webApiService.postResult.subscribe((data: PostData) => console.log(data));
  }

  /**
   * создаем формконтролы для полей формы
   * @param config
   * @protected
   */
  protected buildingFormControls(config: FormConfigItem[]): FormControlItem {
    return config.reduce(
      (accumulator, item): FormControlItem => {
        const validators = this.constructorValidators(item.validators);
        accumulator[item.name] = new FormControl(item.value, [...validators]);
        return accumulator;
      },
      {}
    );
  }

  /**
   * отправка всей формы
   */
  public onSubmit(): void {
    console.log(this.form);
  }

  /**
   * создаем валидаторы для полей
   * @param validatorsList
   * @protected
   */
  protected constructorValidators(validatorsList = []): ValidatorFn[] {
    return validatorsList.reduce((accumulator, item) => {
      const {
        name,
        value,
      } = item;
      if (name && value) {
        accumulator.push(Validators[name](value));
      } else {
        accumulator.push(Validators[name]);
      }
      return accumulator;
    }, []);
  }

  /**
   * отлавливаем изменение полей
   * @param event
   * @param controlName
   * @param controlType
   */
  public onChangeValueInField(event, controlName, controlType): void {
    const value = this.normalizeEventData(event, controlType, controlName);
    const controlValid = this.form.controls[controlName].valid;
    this.webApiService.postFormToServer({
        value,
        controlName,
        controlType,
        controlValid,
        formName: this.formName
      }
    );
  }

  /**
   * выбираем значение для отправки на севрер
   * @param event - событие из DOM
   * @param controlType - тип форм контрола (потребуется что бы определить откуда берем данные из event или this.form)
   * @param controlName - имя форм контрола (потребуется для выборки данных из this.form по имени)
   * @protected
   */
  protected normalizeEventData(event: any, controlType: ConfigTypeField, controlName: string): any {
    let value: any = null;
    switch (controlType) {
      case 'string':
        value = event.target.value;
        break;

      case 'checkbox':
      case 'radio':
      case 'mask':
        value = this.form.value[controlName];
        break;

      case 'number':
        // tslint:disable-next-line:max-line-length
        this.form.get(controlName).patchValue(event.value); // исправление для p-inputNumber, не корректно отрабатывает изменение значений при валидации
        value = this.form.value[controlName];
        break;
      case 'dropdown':
        value = event.value;
        break;

      case 'calendar':
        value = event;
    }
    return value;
  }


}
