import { FormConfigItem } from '../share/interfaces/form-config-item';

export const formName = 'example-form-prototype';

export const formConfig: FormConfigItem[] = [
  {
    width: 12,
    label: 'USER CALENDAR',
    value: new Date(),
    type: 'calendar',
    name: 'userCalendar',
  },
  {
    label: 'USER NAME',
    value: 'Qwerty Qwerty',
    type: 'string',
    name: 'userName',
    width: 3,
    validators: [
      {name: 'required'},
      {name: 'minLength', value: 5}
    ]
  },
  {
    label: 'ID USER',
    value: 10,
    type: 'number',
    name: 'idUser',
    width: 3,
    validators: [
      {name: 'required'}
    ]
  },
  {
    label: 'USER GENDER',
    value: '3',
    type: 'dropdown',
    name: 'userGender',
    width: 3,
    validators: [
      {name: 'required'},
    ],
    list: [
      {label: 'choose answer', value: null},
      {label: 'male', value: '1'},
      {label: 'female', value: '2'},
      {label: 'other', value: '3'}
    ]
  },
  {
    width: 3,
    label: 'USER ANSWER',
    value: 'Y',
    type: 'dropdown',
    name: 'userAnswer',
    list: [{label: 'choose answer', value: null}, {label: 'Yes', value: 'Y'}, {label: 'No', value: 'N'}]
  },

  {
    width: 12,
    label: 'USER MASK',
    value: '(363) 456-7890',
    type: 'mask',
    name: 'userMask',
    mask: '(999) 999-9999',
    validators: [
      {
        name: 'pattern',
        value: '\\([0-9]{3}\\) [0-9]{3}-[0-9]{4}'
      },
    ],
  },

  {
    width: 6,
    label: 'USER CALENDAR 2',
    value: null,
    type: 'calendar',
    name: 'userCalendar2',
    validators: [
      {name: 'required'},
    ],
  },
  {
    width: 6,
    label: 'USER MASK 2',
    value: '12-34',
    type: 'mask',
    name: 'userMask2',
    mask: '99-99',
    validators: [{
      name: 'pattern',
      value: '[0-9]{2}-[0-9]{2}'
    }]
  },
  {
    label: 'SELECT GENDER RADIO',
    value: 2,
    type: 'radio',
    name: 'userGenderRadio',
    width: 3,
    list: [{label: 'male', value: 1}, {label: 'female', value: 2}, {label: 'other', value: 3}]
  },
  {
    label: 'SELECT GENDER CHECKBOX',
    value: [1, 2],
    type: 'checkbox',
    name: 'userGenderCheckbox',
    width: 3,
    list: [{label: 'male', value: 1}, {label: 'female', value: 2}, {label: 'other', value: 3}]
  },
];
